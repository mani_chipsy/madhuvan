$(function() {

    $.items = {
        init: function(callback) {
            $(document).on('click', '[name="btnRowEdit"]', function() {
                $.items.edit($(this));
            });

            $(document).on('click', '[name=btnupdate]', function() {
                $.items.update($(this));
            });

             $(document).on('click', '[name=btnsave]', function() {
              $.items.insert($(this));
            });
             $(document).on('keyup', 'input', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
             $(document).on('change', 'input', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
              $(document).on('change', 'select', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
            
            $(document).on('click', '[name=btncancel]', function() {
                $.items.back($(this));
            });
             $(document).on('click', '[name=btnRowdelete]', function() {
                $.items.delete($(this));
            });

        },
        display: function(offset) {
            offset = parseInt(offset) ? offset : '1';
            //display companys
            // $.loader.start();
            
            $.get('/superadmin/get-data?page=' + offset, function(response) {
                // $.loader.stop();
                var display = "";
                
                for (var i = 0; i < response.data.length; i++) {
                    display += '<tr id=' + response.data[i].id + '><td >' + 
                        parseInt(parseInt(i + 1) + parseInt((offset - 1) * 10)) +'</td> '+
                        '<td > '+response.data[i].name+'</td>' +
                        '<td > '+response.data[i].cat.name+'</td>' +
                        '<td >  Rs. '+response.data[i].price+'</td>' +
                        '<td > '+response.data[i].create+'</td>' +
                        '<td >'+
                        ' <a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>'+
                        '<a href="javascript:void(0);" name="btnRowdelete" class="" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a>'+
                        '</td>'+
                        '</tr>';
                }

                display = display == "" ? '<tr><th colspan=4 class="text-center">No boxes found </th></tr>' : display;

                // alert(display);
                $('[name="boxes"] tbody').html(display);
            }, 'json');
        },
        query_params: function(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },
        insert: function(ths) {
           var proceed = true;

            $('[name=add_item] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });

            if (proceed) {
                var postdata = new FormData();
                postdata.append('itemname', $('#itemname').val());
                postdata.append('price', $('#price').val());
                postdata.append('category', $('#category').val());
                postdata.append('main_category', $('#main_category').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/additem/",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.success) {
                            
                            
                            var display = response.message;

                          
                        $('form[name=add_item]').get(0).reset();
                        swal("Success!", 'Successfully updated', "success");
                        $.items.display($.items.query_params('page'));
                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = key.replace("_", " ").toUpperCase() + ' ' + value[0];
                            //     });
                            // } else {
                            //     display = response.message;
                            // }

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'Error',
                            //             btnClass: 'btn-red',
                            //             action: function() {}
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        }
                    }
                });
            }
        },
        update: function(ths) {
            var id = ths.attr('id');

            var proceed = true;

            $('[name=box-insert] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
        
            if (proceed) {
                    swal({   
                title: "Are you sure?",   
                text: "You will  be able to Update Your item ",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, update it!",   
                closeOnConfirm: false 
            }, function(){
                var postdata = new FormData();
                postdata.append('itemname', $('#itemname').val());
                postdata.append('price', $('#price').val());
                postdata.append('category', $('#category').val());
                 postdata.append('main_category', $('#main_category').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/itemupdate/" + id,
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                         if (response.success) {
                            // $('#add-box').modal('hide');
                            // $.items.display($.items.query_params('page'));
                            
                            var display = response.message;

                             $('form[name=add_item]').get(0).reset();
                            $('.form-actions span').html('<button type="button" class="btn btn-success" name="btnsave"> <i class="fa fa-check"></i> Save</button>');
                                              


                             swal("Success!", 'Successfully updated', "success");
                               $.items.display($.items.query_params('page'));

                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = key.replace("_", " ").toUpperCase() + ' ' + value[0];
                            //     });
                            // } else {
                            //     display = response.message;
                            // }

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'Error',
                            //             btnClass: 'btn-red',
                            //             action: function() {}
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        }
                    }
                });
            
      });
}
        },
        edit: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();

            $.ajax({
                url: '/superadmin/edititem/' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {
                    // $.loader.stop();
                                 
                    $('#itemname').val(response.name);
                    $('#price').val(response.price);
                    $('#category').val(response.category_id);
                    $('#main_category').val(response.main_category);
                    
                    $('.form-actions span').html('<button type="button" class="btn btn-success" id="'+response.id+'" name="btnupdate"> <i class="fa fa-check"></i> Update</button>');
                
                },
                error: function() { /* error code goes here*/ }
            });
        },
         delete: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to Delete Item",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
            $.ajax({
                url: '/superadmin/delete-item/' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {
                    ths.closest('tr').hide();
                      
                            setTimeout(function() {
                               ths.closest('tr').css('background-color', 'rgb(181, 117, 117)').hide(1000); 
                            }, 1001);
                            // display = response.message;
                            display = "Item  has been deleted.";

                            swal("Success!", display, "success");
                            $.billings.display($.billings.query_params('page'));

                }

            });
            });
        },
        back: function($this) {
            $('form[name=add_item]').get(0).reset();
            $('.form-actions span').html('<button type="button" class="btn btn-success" name="btnsave"> <i class="fa fa-check"></i> Save</button>');
        }
    }
    $.items.init();
}(jQuery));