$(function() {

    $.category = {
        init: function(callback) {
            $(document).on('click', '[name="btnRowEdit"]', function() {
                $.category.edit($(this));
            });

            $(document).on('click', '[name=btnRowupdate]', function() {
                $.category.update($(this));
            });

             $(document).on('click', '[name=btnsave]', function() {

              $.category.insert($(this));
            });
             $(document).on('keyup', 'input', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
             $(document).on('change', 'input', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
              $(document).on('change', 'select', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
            
            $(document).on('click', '[name=btnRowcancel]', function() {
                $.category.back($(this));
            });
             $(document).on('click', '[name=btnRowdelete]', function() {
                $.category.delete($(this));
            });

        },
      
        query_params: function(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },
        insert: function(ths) {
           var proceed = true;

      
                if ( $('#category1').val() == "") {
                   $('#category1').css('border-color', 'red');
                    proceed = false;
                }
       

            if (proceed) {
                var postdata = new FormData();
                postdata.append('category', $('#category1').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/category/add",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.success) {
                            
                            
                            var display = response.message;

                          // var newRowContent = "<tr><td><input type=\"checkbox\" id=\"" + chkboxId + "\" value=\"" + chkboxValue + "\"></td><td>" + displayName + "</td><td>" + logicalName + "</td><td>" + dataType + "</td><td><input type=\"checkbox\" id=\"chkAllPrimaryAttrs\" name=\"chkAllPrimaryAttrs\" value=\"chkAllPrimaryAttrs\"></td><td><input type=\"checkbox\" id=\"chkAllPrimaryAttrs\" name=\"chkAllPrimaryAttrs\" value=\"chkAllPrimaryAttrs\"></td></tr>";

                          //   $("tbody").append(newRowContent); 
                        // $('form[name=add_item]').get(0).reset();
                        swal("Success!", 'Successfully updated', "success");
                    location.reload();
                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = key.replace("_", " ").toUpperCase() + ' ' + value[0];
                            //     });
                            // } else {
                            //     display = response.message;
                            // }

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'Error',
                            //             btnClass: 'btn-red',
                            //             action: function() {}
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        }
                    }
                });
            }
        },
        update: function(ths) {
            var id = ths.closest('tr').attr('id');

            var proceed = true;

            $('[name=box-insert] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
        
            if (proceed) {
                    swal({   
                title: "Are you sure?",   
                text: "You will  be able to Update  Category",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, update it!",   
                closeOnConfirm: false 
            }, function(){
                var postdata = new FormData();
                postdata.append('category',ths.closest('tr').find('input').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/category/update/" + id,
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                         if (response.success) {
                            // $('#add-box').modal('hide');
                            // $.category.display($.category.query_params('page'));
                            
                            var display = response.message;

                             ths.closest('tr').find('input').attr("readonly", true); 
                                   ths.closest('tr').find('input').attr("class","input_hide");        
                            ths.closest('tr').find('.text-nowrap').html('<a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="javascript:void(0);" name="btnRowdelete" class="" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a>');
                             swal("Success!", 'Successfully updated', "success");
                               // $.category.display($.category.query_params('page'));
                                

                        } else {
                            

                        }
                    }
                });
            
      });
}
        },
        edit: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();
             ths.closest('tr').find('input').attr("readonly", false);
              ths.closest('tr').find('input').attr("class","form-control");

              ths.closest('tr').find('.text-nowrap').html('<a href="javascript:void(0);" name="btnRowupdate" data-toggle="tooltip" data-original-title="Edit"> <i class="fa  fa-check-square-o text-inverse m-r-10"></i> </a><a href="javascript:void(0);" name="btnRowcancel" class="" data-toggle="tooltip" data-original-title="Cancel"> <i class="fa fa-remove text-inverse m-r-10"></i> </a>');
         
        },
         delete: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();
            swal({   
                title: "Are you sure?",   
                text: "You Want Delete Category",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
            $.ajax({
                url: '/superadmin/category/delete/' + id,
                type: "DELETE",
                dataType: "json",

                success: function(response) {
                    ths.closest('tr').hide();
                      
                            setTimeout(function() {
                               ths.closest('tr').css('background-color', 'rgb(181, 117, 117)').hide(1000); 
                            }, 1001);
                            // display = response.message;
                            display = "Category has been deleted.";

                            swal("Success!", display, "success");
                            $.billings.display($.billings.query_params('page'));

                }

            });
            });
        },
        back: function(ths) {
          ths.closest('tr').find('input').attr("readonly", true);  
             ths.closest('tr').find('input').attr("class","input_hide");      
          ths.closest('tr').find('.text-nowrap').html('<a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="javascript:void(0);" name="btnRowdelete" class="" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a>');
                             
        }
    }
    $.category.init();
}(jQuery));