$(function() {

    $.floor = {
        init: function(callback) {
            $(document).on('click', '[name="btnRowEdit"]', function() {
                $.floor.edit($(this));
            });

            $(document).on('click', '[name=btnRowupdate]', function() {
                $.floor.update($(this));
            });

             $(document).on('click', '[name=btnsave]', function() {

              $.floor.insert($(this));
            });
             $(document).on('keyup', 'input', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
             $(document).on('change', 'input', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
              $(document).on('change', 'select', function() {
                // alert("hii");
                $(this).css('border','1px solid #ced4da');
            });
            
            $(document).on('click', '[name=btnRowcancel]', function() {
                $.floor.back($(this));
            });
             $(document).on('click', '[name=btnRowdelete]', function() {
                $.floor.delete($(this));
            });

        },
      
        
        insert: function(ths) {
           var proceed = true;

                
            $('[name=floor] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
                
       

            if (proceed) {
                var postdata = new FormData();
                postdata.append('name', $('#floorname').val());
                postdata.append('noOfSeats', $('#seats').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/floor-details/add",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.success) {
                            
                            
                            var display = response.message;

                          // var newRowContent = "<tr><td><input type=\"checkbox\" id=\"" + chkboxId + "\" value=\"" + chkboxValue + "\"></td><td>" + displayName + "</td><td>" + logicalName + "</td><td>" + dataType + "</td><td><input type=\"checkbox\" id=\"chkAllPrimaryAttrs\" name=\"chkAllPrimaryAttrs\" value=\"chkAllPrimaryAttrs\"></td><td><input type=\"checkbox\" id=\"chkAllPrimaryAttrs\" name=\"chkAllPrimaryAttrs\" value=\"chkAllPrimaryAttrs\"></td></tr>";

                          //   $("tbody").append(newRowContent); 
                        // $('form[name=add_item]').get(0).reset();
                        swal("Success!", 'Successfully updated', "success");
                    location.reload();
                        } else {
                         

                        }
                    }
                });
            }
        },
        update: function(ths) {
            var id = ths.closest('tr').attr('id');

            var proceed = true;
         
           
                if (ths.closest('tr').find('#noOfSeats').val() == "") {
                  ths.closest('tr').find('#noOfSeats').css('border-color', 'red');
                    proceed = false;
                }
         
                if (ths.closest('tr').find('#name').val() == "") {
                    ths.closest('tr').find('#name').css('border-color', 'red');
                    proceed = false;
                }
         
            if (proceed) {
                    swal({   
                title: "Are you sure?",   
                text: "You will  be able to Update  Floor Details",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, update it!",   
                closeOnConfirm: false 
            }, function(){
                var postdata = new FormData();
                
                postdata.append('noOfSeats',ths.closest('tr').find('#noOfSeats').val());
                postdata.append('name',ths.closest('tr').find('#name').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/floor-details/update/" + id,
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                         if (response.success) {
                          
                            
                            var display = response.message;

                             ths.closest('tr').find('input').attr("readonly", true); 
                              ths.closest('tr').find('input').attr("class","input_hide");       
                            ths.closest('tr').find('.text-nowrap').html('<a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="javascript:void(0);" name="btnRowdelete" class="" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a>');
                             swal("Success!", 'Successfully updated', "success");
                              
                                

                        } else {
                            

                        }
                    }
                });
            
      });
}
        },
        edit: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();
             ths.closest('tr').find('input').attr("readonly", false);
             ths.closest('tr').find('input').attr("class","form-control");
             
              ths.closest('tr').find('.text-nowrap').html('<a href="javascript:void(0);" name="btnRowupdate" data-toggle="tooltip" data-original-title="Edit"> <i class="fa  fa-check-square-o text-inverse m-r-10"></i> </a><a href="javascript:void(0);" name="btnRowcancel" class="" data-toggle="tooltip" data-original-title="Cancel"> <i class="fa fa-remove text-inverse m-r-10"></i> </a>');
         
        },
         delete: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();
            swal({   
                title: "Are you sure?",   
                text: "You Want Delete Category",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
            $.ajax({
                url: '/superadmin/floor-details/delete/' + id,
                type: "DELETE",
                dataType: "json",

                success: function(response) {
                    ths.closest('tr').hide();
                      
                            setTimeout(function() {
                               ths.closest('tr').css('background-color', 'rgb(181, 117, 117)').hide(1000); 
                            }, 1001);
                            // display = response.message;
                            display = "Category has been deleted.";

                            swal("Success!", display, "success");
                            $.billings.display($.billings.query_params('page'));

                }

            });
            });
        },
        back: function(ths) {
          ths.closest('tr').find('input').attr("readonly", true); 
          ths.closest('tr').find('input').attr("class","input_hide");     
          ths.closest('tr').find('.text-nowrap').html('<a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="javascript:void(0);" name="btnRowdelete" class="" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a>');
                             
        }
    }
    $.floor.init();
}(jQuery));