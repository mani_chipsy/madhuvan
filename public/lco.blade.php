@extends('Admin.layouts.master_layout')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">LCOs</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">LCOs</li>
            </ol>
        </div>
    </div>
    <div class="row" name="lco-display">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Total ({{$lcos->total()}})
                    <span class="pull-right">
                        <div class="button-group">
                            <button type="button" class="btn btn-secondary btn-sm" onclick='location.href="/superadmin/lcos/download/sample_template.xls"'>Sample Template</button>
                            <button type="button" class="btn btn-warning btn-sm" name="showImport">Import LCOs</button><button class="btn btn-info btn-sm" name="showForm">Add {{--<i class="fa fa-plus"></i>--}}</button>
                        </div>
                    </span>
                    </h4>
                    <div id="upload-lco" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Action</h4> 
                                    <a class="close pull-right" data-dismiss="modal" aria-hidden="true">×</a></div>
                                <div class="modal-body">
                                    <div name='lco-uplod'>
                                        <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="fileupload btn btn-danger btn-rounded waves-effect waves-light"><span><i class="ion-upload m-r-5"></i>Upload File</span>
                                                <input type="file" id='lco_file' name="lco_file" class="upload" required="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 8px;">
                                            <span name="js_lco_name" ></span>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info waves-effect" name="btnImport" >Save</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                   <div class="table-responsive">
                                    <table class="table color-table success-table" name="lcos">
                      <!--   <table class="table table-bordered" > -->
                            <thead>
                                <tr>
                                    <th>Sl. No.</th>
                                    <th>LCO No.</th>
                                    <th>Group Name</th>
                                    <th>Name</th>
                                    <th>Mob No.</th>
                                    <th>Email</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php $i=1; @endphp
                                    
                            @foreach ($lcos as $lco)
                                <tr id="{{$lco->id}}">
                                    <td>{{ (( $lcos->currentPage() - 1) * 10 ) + $i++ }}</td>
                                    <td>{{$lco->lco_num}}</td>
                                    <td>{{$lco->group->name}}</td>
                                    <td>{{$lco->name}}</td>
                                    <td>{{$lco->mobile}}</td>
                                    <td>{{$lco->email}}</td>
                                    <td class="text-nowrap">
                                        <a href="javascript:void(0);" name="btnRowView" data-toggle="tooltip" data-original-title="View details"> <i class="fa fa-eye text-inverse m-r-10"></i></a>
                                        <a href="javascript:void(0);" name="btnRowMap" id="{{$lco->id}}" > <i class="fa fa-sitemap text-inverse m-r-10"></i></a>
                                        <a href="javascript:void(0);"  name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i></a>
                                        <a href="javascript:void(0);" name="btnRowDelete" class="text-danger" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @if (!$lcos->count())
                            <tr><td  > No LCOs found</td></tr>
                            @endif
                            </tbody>
                        </table>
                        <ul class="pagination pagination-sm no-margin pull-right"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="add-lco" style="display: none">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="m-b-0 text-white" name="form-title"></h4>
                </div>
                <div class="card-body">
                    <form name="lco-insert">
                        <div class="form-body">
                            <h3 class="card-title" style="font-weight: 600;">Personal Info</h3>
                            <hr>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">LCO Number</label>
                                        <input type="text" id="lco_num" class="form-control form-control-danger" required="true" placeholder="">
                                        <small class="form-control-feedback"></small> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">LCO Name</label>
                                        <input type="text"  required="true" id="lco_name" class="form-control form-control-danger" placeholder="">
                                        <small class="form-control-feedback"></small> </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Group</label>
                                        <select required="true" class="form-control custom-select" id="group_id">
                                            <option value="">Select Group</option>
                                            @foreach (Group::get() as $group)
                                            <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                        <small class="form-control-feedback"></small> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">GST No</label>
                                        <input type="text" id='gst_no' class="form-control" placeholder="">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" id='email' class="form-control" placeholder="">
                                        
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Mobile</label>
                                        <input type="text" id='mobile' class="form-control" placeholder="">
                                        
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <h3 class="box-title m-t-40" style="font-weight: 600;">Address</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" id="address" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" id='city' class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>District</label>
                                        <input type="text" id="district" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>PIN Code</label>
                                        <input type="text" id="pin_code" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">&nbsp;</div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" class="btn btn-success" name="btninsert"> <i class="fa fa-check"></i> Save</button>
                            <button type="button" class="btn btn-success" name="btnupdate" style="display: none"> <i class="fa fa-save"></i> Update</button>
                            <button type="button" class="btn btn-inverse" name="btncancel">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="viewModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <th>LCO No.</th>
                            <td><span id="lco_num"></span></td>
                        </tr>
                        <tr>
                            <th>GST No.</th>
                            <td><span id="gst_no"></span></td>
                        </tr>
                        <tr>
                            <th>Name.</th>
                            <td><span id="lco_name"></span></td>
                        </tr>
                        <tr>
                            <th>Group.</th>
                            <td><span id="group"></span></td>
                        </tr>
                        <tr>
                            <th>Mobile.</th>
                            <td><span id="mobile"></span></td>
                        </tr>
                        <tr>
                            <th>Email.</th>
                            <td><span id="email"></span></td>
                        </tr>
                        <tr>
                            <th>Address.</th>
                            <td><span id="address"></span></td>
                        </tr>
                        <tr>
                            <th>City.</th>
                            <td><span id="city"></span> <span id="pin_code"></span></td>
                        </tr>
                        <tr>
                            <th>District.</th>
                            <td><span id="district"></span></td>
                        </tr>

                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1" style="font-weight: 600;">Lco Boxes</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <form name="boxes">
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">SD BOXES:</label>
                                                        <input type="text" class="form-control" id="sd_box" name="sd_box" required="true">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">HD-SD BOXES:</label>
                                                        <input type="text" class="form-control" id="hd_sd_box" name="hd_sd_box" required="true">
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="recipient-name" class="control-label">HD-HD BOXES:</label>
                                                        <input type="text" class="form-control" id="hd_hd_box" name="hd_hd_box" required="true">
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" id="" name="send">Send message</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
</div>
<script type="text/javascript" src='/admin/jscontrols/lcos.js'></script>
<script>var currentpage = 1;</script>
<script type="text/javascript">
 $(document).on('click', '[name=btnRowMap]',function(){
                var id = $(this).attr("id");
                 $('form[name=boxes]').get(0).reset();
             $('[name=send]').attr('id',id); 
             $.ajax({
                url: '/superadmin/lcos/lco-boxes/' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {
                    // alert("hii");
                    // alert(JSON.stringify(response));
                    // alert(response['0'].id);
                   
                    $('#exampleModal').find('#sd_box').val(response['0'].box_qty);
                    $('#exampleModal').find('#hd_sd_box').val(response['1'].box_qty);
                    $('#exampleModal').find('#hd_hd_box').val(response['2'].box_qty);
                    

                    // $("#viewModal").modal('show');
                },
                error: function() { /* error code goes here*/ }
            });
        
                     
 $('#exampleModal').modal('show');
  });
</script>

<script> 
  $('.pagination').twbsPagination({
      totalPages: Math.ceil("{{ $lcos->total() / 10 }}"),
      startPage:parseInt("{{ $lcos->currentPage() }}"),
      initiateStartPageClick:false,
      visiblePages: 10,
      onPageClick: function (event, page) {
        var getdata = {};
        getdata['page'] = page;
        var display = "";
        // $.loader.start(); 
        $.get('/superadmin/lcos/get-data',getdata,function(response){
          var display = "";
          for(var i=0; i<response.data.length; i++){
            // $.loader.stop();
             display += '<tr id=' + response.data[i].id + '><td >' + 
                        parseInt(parseInt(i + 1) + parseInt((page - 1) * 10)) +'</td> '+
                        '<td > '+response.data[i].lco_num+'</td>' +
                        '<td > '+response.data[i].group.name+'</td>' +
                        '<td > '+response.data[i].name+'</td>' +
                        '<td > '+response.data[i].mobile+'</td>' +
                        '<td >' +response.data[i].email + '</td><td ><a href="javascript:void(0);" name="btnRowView" data-toggle="tooltip" data-original-title="View details"> <i class="fa fa-eye text-inverse m-r-10"></i></a>'+
                        '<a href="javascript:void(0);" name="btnRowMap" data-toggle="tooltip" data-original-title="Assign Boxes"> <i class="fa fa-sitemap text-inverse m-r-10"></i></a>'+
                        '<a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i></a>'+
                        '<a href="javascript:void(0);" name="btnRowDelete" class="text-danger" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i></a>'+
                        '</td>'+
                        '</tr>';
          }

          $('table[name=lcos] tbody').html(display);
        
      },'json');

        history.pushState('', '', '/superadmin/lcos?page=' + page);
        currentpage = page;
      }
  });
</script>
@endsection