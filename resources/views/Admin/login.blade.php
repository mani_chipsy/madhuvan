
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Madhuvan Admin | Login</title>
        <!-- Favicon-->
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <!-- Bootstrap Core Css -->
        <link href="/assests/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Waves Effect Css -->
        <link href="/assests/plugins/node-waves/waves.css" rel="stylesheet" />
        <!-- Animation Css -->
        <link href="/assests/plugins/animate-css/animate.css" rel="stylesheet" />
        <!-- Morris Chart Css-->
        <!-- Custom Css -->
        <link href="/assests/css/style.css" rel="stylesheet">
         <script src="/assests/js/jquery.min.js"></script>
         <script src="/assests/lib/angular/angular.min.js"></script>
        <script src="/assests/lib/angular/route.js"></script>
    </head>
    <body class="login-page login-content" ng-app="TapmiAdmin" >
        <div class="login-box" ng-controller="LoginController">            
            <div class="card">
                <div class="logo">
                    <a href="javascript:void(0);"><span style="font-size: 24px;font-weight: 900;color: #e52315">M</span></a>
                    <small><b>Madhuvan </b></small>
                </div>
                <div class="body" ng-controller="LoginController">
                    <form id="sign_in" method="POST" action=""  name="login-inputs" >
                        <input type="hidden" name="_token" value="1Vg9ztkT4d7d7srf9BeKDjxUy3Baba0qU7f0QMn7">                    
                        <div class="msg"><b>Signin to start your session</b></div>
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" placeholder="Username" name="username" required="true" autofocus>
                               
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" placeholder="Password" name="password" required="true">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-5">          
                                <div name="message_area">
                                    <label></label>
                                </div>  
                            </div>
                            <div class="col-xs-4">
                                <button class="btn btn-block bg-pink tap-btn waves-effect" type="button"  name="submit-login">SIGN IN</button>
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
  <script src="/admin/jscontrols/app_controls.js"></script>
        <script src="/admin/jscontrols/login_controls.js"></script>
        <script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>
        
       

    </body>
</html>
