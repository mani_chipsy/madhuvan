@extends('Admin.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/admin/css/jquery-ui.min.css">
<style type="text/css">
  [name='lcos-search'] {
    text-transform: uppercase;
  }
</style>
<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="row page-titles">
    <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor">Dashboard </h3>
    </div>
    <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/superadmin/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Dashboard </li>
      </ol>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Stats box -->
  <!-- ============================================================== -->
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          Hello world
        </div>
      </div>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Right sidebar -->
  <!-- ============================================================== -->
</div>
<script type="text/javascript" src='/admin/jscontrols/home.js'></script>
<script type="text/javascript" src="/admin/jscontrols/jquery-ui.js"></script>
<script type="text/javascript">
  var getData = function(request, response) {
        $.getJSON('/superadmin/lcos/search/' + request.term, function(data) {
            response(data);
        });
    };

    var selectItem = function(event, ui) {
       ui.item.label =  ui.item.label.replace("LCOBL", "");
      $('[name="lcos-id"]').val(ui.item.value);
      $('[name="lcos-id"]').trigger('change');
      $('[name="lcos-search"]').val(ui.item.label);
      return false;
    };

    var focusItem = function(event, ui) {
        event.preventDefault();
    }

    $("[name='lcos-search']").autocomplete({
        source: getData,
        select: selectItem,
        focus: focusItem,
        minLength: 1
    });
</script>


@endsection