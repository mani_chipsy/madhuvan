@extends('Admin.layouts.master_layout')
@section('content')
<div class="container-fluid">
   <!-- ============================================================== -->
   <!-- Bread crumb and right sidebar toggle -->
   <!-- ============================================================== -->
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor">Floor Details</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/superadmin/dashboard">Home</a></li>
            <li class="breadcrumb-item active">Floor Details</li>
         </ol>
      </div>
   </div>
   <div class="row">
     <div class="col-md-8 ">
    <div class="card">
            <div class="card-body">
               <h4 class="card-title"></h4>
               <!-- /.box-header -->
               <div class="table-responsive">
                  <table class="table table-hover" name="boxes">
                     <thead>
                        <tr >
                           <th>Sl_no</th>
                          
                           <th>Floor Name</th>
                           <th>Number Of Seats</th>
                          
                           <th colspan="2">Actions</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $i=1; @endphp 
                     @foreach ($items as $item)
                     <tr id="{{$item->id}}" name="floordetails">
                     <td>{{$i++}}</td>
                     <td><input type="text" name="name" id="name" class=" input_hide" readonly="true" value="{{$item->name}}" ></td>
                       <td><input type="text" id="noOfSeats" name="noOfSeats" class="input_hide" readonly="true" value="{{$item->noOfSeats}}" ></td>
                     <td  class="text-nowrap"><a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                           <a href="javascript:void(0);" name="btnRowdelete" class="" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a></td>
                     </tr>
                      @endforeach
                     @if (!$items->count())
                     <tr>
                        <td  > No Itemes found</td>
                     </tr>
                     @endif

                   
                     <tr name="floor">
                     <td>#</td>
                    
                     <td>
                     <input type="text" name="floorname" id="floorname" class="form-control" required="true" value="" ></td>
                       <td>
                     <input type="text" name="seats" id="seats" class="form-control" required="true" value="" ></td>
                     <td><div class="input-group-append">
                     <button class="btn btn-success" name="btnsave" type="button" ><i class="fa fa-plus"></i></button>
                      </div></td>
                      </tr>
                     
                     </tbody>
                     </table>
                     </div>
                     </div>
                     </div>
                     </div>
                     </div>

<script type="text/javascript" src='/admin/jscontrols/floor.js'></script>

<style type="text/css">
   .input_hide{
     border: none;
         background-color: transparent!important;
             opacity: 1!important;
             color: #67757c;
    min-height: 38px;
    display: initial;
        padding: .375rem .75rem;
    font-size: 1rem;
}
</style>
                     @endsection