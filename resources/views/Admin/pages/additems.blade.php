@extends('Admin.layouts.master_layout')
@section('content')
<div class="container-fluid">
   <!-- ============================================================== -->
   <!-- Bread crumb and right sidebar toggle -->
   <!-- ============================================================== -->
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor">Item Details</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/superadmin/dashboard">Home</a></li>
            <li class="breadcrumb-item active">Item Details</li>
         </ol>
      </div>
   </div>
   <div class="row">
      <div class="col-md-6">
         <div class="card">
            <div class="card-body">
               <!-- general form elements -->
               <div class="box box-primary">
                  <div class="box-header with-border">
                     <h3 class="box-title">Item Creation</h3>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <form role="form"  name='add_item'>
                     <div class="box-body">
                        {{csrf_field()}}
                        <div class="form-group">
                           <label for="user" >Item Name</label>
                           <input type="text"  id="itemname" required="true" name="itemname" class="form-control" placeholder="Item ">
                        </div>
                        <div class="form-group">
                           <label class="control-label">Main Category</label>
                           <select class="form-control custom-select" id="main_category" name="main_category" data-placeholder="Choose a Category" tabindex="1">
                              @foreach(App\Main_category::get() as $data)
                              <option value="{{$data->id}}">{{$data->name}}</option>
                              @endforeach
                           </select>
                        </div>

                           <div class="form-group">
                           <label class="control-label">Category</label>
                           <select class="form-control custom-select" id="category" name="category" data-placeholder="Choose a Category" tabindex="1">
                              @foreach(App\Category_master::get() as $data)
                              <option value="{{$data->id}}">{{$data->name}}</option>
                              @endforeach
                           </select>
                        </div>


                        <div class="form-group">
                           <label for="user" >Price</label>
                           <input type="text"  id="price" required="true" name="price" class="form-control" placeholder="Price ">
                        </div>
                        <div class="resp-msg"></div>
                        <div class="form-actions">
                           <span><button type="button" class="btn btn-success" name="btnsave"> <i class="fa fa-check"></i> Save</button></span>
                           <button type="button" class="btn btn-inverse" name="btncancel">Cancel</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">Total Item ( {{$items->total()}} )</h4>
               <!-- /.box-header -->
               <div class="table-responsive">
                  <table class="table" name="boxes">
                     <thead>
                        <tr >
                           <th>Sl_no</th>
                           <th>Item Name</th>
                           <th>Category Name</th>
                           <th>Price</th>
                           <th>Create_Date</th>
                           <th colspan="2">Actions</th>
                        </tr>
                     </thead>
                     @php $i=1; @endphp 
                     @foreach ($items as $item)
                     <tr id="{{$item->id}}">
                        <td>{{ (( $items->currentPage() - 1) * 10 ) + $i++ }}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->cat->name}}</td>
                        <td>Rs. {{$item->price}}</td>
                        <td>{{$item->create}}</td>

                        <td class="text-nowrap">
                           <a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                           <a href="javascript:void(0);" name="btnRowdelete" class="" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a>
                        </td>
                     </tr>
                     @endforeach
                     @if (!$items->count())
                     <tr>
                        <td  > No Itemes found</td>
                     </tr>
                     @endif
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
               <div class="box-footer clearfix">
                  <div class=" text-left">
                     <ul uib-pagination total-items="totalUsers" ng-model="currentPage" class="pagination pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="usersPerPage"></ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src='/admin/jscontrols/item.js'></script>
<script>var currentpage = 1;</script>
<script> 
   $('.pagination').twbsPagination({
       totalPages: Math.ceil("{{ $items->total() / 10 }}"),
       startPage:parseInt("{{ $items->currentPage() }}"),
       initiateStartPageClick:false,
       visiblePages: 10,
       onPageClick: function (event, page) {
         var getdata = {};
         getdata['page'] = page;
         var display = "";
         // $.loader.start(); 
         $.get('/superadmin/get-data',getdata,function(response){
           var display = "";
           for(var i=0; i<response.data.length; i++){
             // $.loader.stop();
              display += '<tr id=' + response.data[i].id + '><td >' + 
                         parseInt(parseInt(i + 1) + parseInt((page - 1) * 10)) +'</td> '+
                         '<td > '+response.data[i].name+'</td>' +
                         '<td > '+response.data[i].cat.name+'</td>' +
                         '<td > Rs. '+response.data[i].price+'</td>' +
                         '<td > '+response.data[i].create+'</td>' +
                         '<td >'+
                         ' <a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>'+
                         '<a href="javascript:void(0);" name="btnRowdelete" class="" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a>'+
                         '</td>'+
                         '</tr>';
           }
   
           $('table[name=boxes] tbody').html(display);
         
       },'json');
   
         history.pushState('', '', '/superadmin/get-data?page=' + page);
         currentpage = page;
       }
   });
</script>
@endsection