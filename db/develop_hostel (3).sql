-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 20, 2018 at 11:03 AM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `develop_hostel`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_master`
--

CREATE TABLE `category_master` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `ordering` int(5) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_master`
--

INSERT INTO `category_master` (`id`, `name`, `status`, `ordering`, `created_at`, `updated_at`) VALUES
(1, 'Food & Beverages', 1, 0, '2018-07-14 00:00:00', '2018-07-14 00:00:00'),
(5, 'Liquor', 1, 0, '2018-07-14 00:00:00', '2018-07-14 00:00:00'),
(6, 'Desserts', 1, 0, '2018-07-14 00:00:00', '2018-07-14 00:00:00'),
(47, 'Snacks', 1, 1, NULL, NULL),
(48, 'Starters', 1, 1, NULL, NULL),
(49, 'Salads', 1, 1, NULL, NULL),
(50, 'Mains', 1, 1, NULL, NULL),
(51, 'Juices', 1, 1, NULL, NULL),
(52, 'Mocktails', 1, 1, NULL, NULL),
(53, 'Soup', 1, 1, NULL, NULL),
(54, 'Cocktails', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `floor_details`
--

CREATE TABLE `floor_details` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `noOfSeats` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floor_details`
--

INSERT INTO `floor_details` (`id`, `name`, `noOfSeats`, `created_at`, `updated_at`) VALUES
(1, 'Floor one', 15, '2018-07-09 00:00:00', '2018-07-11 00:00:00'),
(2, 'Floor Two', 12, '2018-07-14 00:00:00', '2018-07-14 00:00:00'),
(3, 'Floor Three', 11, '2018-07-14 00:00:00', '2018-07-14 00:00:00'),
(4, 'Floor Four', 10, '2018-07-14 00:00:00', '2018-07-14 00:00:00'),
(5, 'Floor Five', 15, '2018-07-14 00:00:00', '2018-07-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

CREATE TABLE `item_master` (
  `id` int(11) NOT NULL,
  `main_category` int(10) NOT NULL,
  `category_id` tinyint(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` tinyint(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_master`
--

INSERT INTO `item_master` (`id`, `main_category`, `category_id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(14, 1, 49, 'Venilla', 50, '2018-07-18 08:01:57', '2018-07-18 09:04:23'),
(15, 1, 53, 'Chiken', 50, '2018-07-18 08:02:17', '2018-07-18 08:02:17'),
(16, 2, 54, 'Egg', 85, '2018-07-18 08:03:01', '2018-07-19 05:56:43'),
(17, 1, 5, 'Cabab', 127, '2018-07-19 05:56:02', '2018-07-19 05:56:02');

-- --------------------------------------------------------

--
-- Table structure for table `main_category`
--

CREATE TABLE `main_category` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_category`
--

INSERT INTO `main_category` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Restaurant', 1, NULL, NULL),
(2, 'Food Item', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `sliptid` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `order_no` int(11) NOT NULL,
  `comment` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `sliptid`, `item_id`, `qty`, `order_no`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 14, 5, 1, NULL, '2018-07-19 06:52:34', '2018-07-19 06:52:34'),
(2, 1, 15, 5, 1, NULL, '2018-07-19 06:53:28', '2018-07-19 06:53:28'),
(3, 1, 16, 5, 2, NULL, '2018-07-19 07:25:35', '2018-07-19 07:25:35'),
(4, 1, 15, 5, 2, NULL, '2018-07-19 07:25:42', '2018-07-19 07:25:42');

-- --------------------------------------------------------

--
-- Table structure for table `slipt_table`
--

CREATE TABLE `slipt_table` (
  `sliptid` int(11) NOT NULL,
  `floorid` int(10) NOT NULL,
  `table_no` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slipt_table`
--

INSERT INTO `slipt_table` (`sliptid`, `floorid`, `table_no`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '2018-07-18', '2018-07-18'),
(2, 0, 1, '2018-07-18', '2018-07-18'),
(3, 0, 1, '2018-07-18', '2018-07-18'),
(4, 0, 1, '2018-07-18', '2018-07-18'),
(5, 0, 1, '2018-07-18', '2018-07-18'),
(6, 0, 1, '2018-07-18', '2018-07-18'),
(7, 0, 1, '2018-07-18', '2018-07-18'),
(8, 0, 2, '2018-07-18', '2018-07-18'),
(9, 0, 2, '2018-07-18', '2018-07-18'),
(10, 0, 2, '2018-07-18', '2018-07-18'),
(11, 0, 2, '2018-07-18', '2018-07-18'),
(12, 0, 2, '2018-07-18', '2018-07-18'),
(13, 1, 2, '2018-07-18', '2018-07-18'),
(14, 1, 2, '2018-07-18', '2018-07-18'),
(15, 3, 1, '2018-07-19', '2018-07-19'),
(16, 3, 1, '2018-07-19', '2018-07-19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roleId` tinyint(1) UNSIGNED NOT NULL COMMENT '1 - Admin, 2- Others',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `roleId`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$Mjd8335RrMmouBwN.8mAvOokhwsFiracg2/GJTSO46FHX6jQWtApK', 1, '2fPy9j2g5xHDQwRfkNIJ5UbWMIgq24Mf4M4l6l8svyQLHEDxu35IokIMV9RI', NULL, '2018-05-19 04:33:40'),
(5, 'user', 'user@gmail.com', '$2y$10$Mjd8335RrMmouBwN.8mAvOokhwsFiracg2/GJTSO46FHX6jQWtApK', 2, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_master`
--
ALTER TABLE `category_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floor_details`
--
ALTER TABLE `floor_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_master`
--
ALTER TABLE `item_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_category`
--
ALTER TABLE `main_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slipt_table`
--
ALTER TABLE `slipt_table`
  ADD PRIMARY KEY (`sliptid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_master`
--
ALTER TABLE `category_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `floor_details`
--
ALTER TABLE `floor_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `item_master`
--
ALTER TABLE `item_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `main_category`
--
ALTER TABLE `main_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `slipt_table`
--
ALTER TABLE `slipt_table`
  MODIFY `sliptid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
