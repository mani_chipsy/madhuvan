<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Orders extends Model
{
    protected $table = 'orders';
   
   public function item()
    {
        return $this->belongsTo('App\Item_master','item_id','id');
    }
    
  
}
