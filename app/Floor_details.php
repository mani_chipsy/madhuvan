<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor_details extends Model
{
     protected $fillable = [
        'name',
        'noOfSeats',
    ];
}
