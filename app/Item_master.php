<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_master extends Model
{
    protected $table = 'item_master';
    protected $fillable = [
        'name' 
    ];


     public function cat()
    {
        return $this->belongsTo('App\Category_master','category_id','id');
    }
     public function main_cat()
    {
        return $this->belongsTo('App\Main_category','main_category','id');
    }
   
}
