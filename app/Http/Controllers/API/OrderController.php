<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use JWTAuth;


class OrderController extends Controller
{
    
    public function floor_controller(Request $request)
    {
        
        $res                           = \App\Floor_details::select('id', 'name', 'noOfSeats')->get();
        $response['status']['code']    = 0;
        $response['status']['message'] = "Listing  floor details";
        $response['data']              = $res;
        return response()->json($response);
        
    }
    
    
    
    public function category_master_controller(Request $request)
    {
        $res                           = \App\Category_master::select('id', 'name')->with('item')->get();
        $response['status']['code']    = 0;
        $response['status']['message'] = "Listing  category details";
        $response['data']              = $res;
        return response()->json($response);
        
    }
    
    public function item_master_controller(Request $request)
    {
        $res                           = \App\Item_master::select('id', 'name', 'price')->get();
        $response['status']['code']    = 0;
        $response['status']['message'] = "Listing  item details";
        $response['data']              = $res;
        return response()->json($response);
        
    }
    
    public function order_controller(Request $request)
    {
        $rules     = array(
            'sliptid' => 'required',
            'item_id' => 'required',
            'qty' => 'required',
            'order_no' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code']    = 1;
            $response['status']['message'] = $validator->messages();
            return response()->json($response);
        }
        $data             = $request->all();
        $orders           = new \App\Orders;
        $orders->sliptid  = $data['sliptid'];
        $orders->item_id  = $data['item_id'];
        $orders->qty      = $data['qty'];
        $orders->order_no = $data['order_no'];
        $orders->comment  = $data['comment'];
        $da               = $orders->save();
        if ($da) {
            $response['status']['code']    = 0;
            $response['status']['message'] = "Order is Updated";
        } else {
            $response['status']['code']    = 0;
            $response['status']['message'] = "Something happens";
        }
        return response()->json($response);
        
    }
     public function  sliptget_controller(Request $request){
      $data = $request->only('table_no','floorid');
      $data['table_no']  = json_decode($data['table_no'], true);
      $data['floorid']   = json_decode($data['floorid'], true);
      
      $data=\App\Slipt_table::select('sliptid','table_no','floorid')->where('table_no',$data['table_no'])->where('floorid',$data['floorid'])->get();
      $response['status']['code']=0;
      $response['status']['message']="Listing  Slipt table details";
      $response['data']=$data;
      return response()->json($response);
   }
    
    public function confirm_order_controller(Request $request)
    {
        $rules     = array(
            'sliptid' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code']    = 1;
            $response['status']['message'] = $validator->messages();
            return response()->json($response);
        }
        $data= $request->all();
        $MyObjects= array();
        $orders= \App\Orders::where('sliptid', $data['sliptid'])->groupBy('order_no')->get();
        $total_price = 0;
        foreach ($orders as $key) {
        $res= \App\Orders::with('item')->where('sliptid', $data['sliptid'])->where('order_no', $key->order_no)->get();
        $price= 0;
        foreach ($res as $key) {
        $price=$key->qty*$key['item']->price;
        $total_price+=$price;
        $key->price=$price;
        }
        $MyObject['order_no']=$key->order_no;
        $MyObject['items']=$res;
        $MyObjects[]=$MyObject;
        }
        $response['status']['code']= 0;
        $response['status']['message']= "Order Detailes";
        $response['data']['order_details']= $MyObjects;
        $response['data']['total_price']= $total_price;
        return response()->json($response);
    }
    
    
    public function  sliptrequest_controller(Request $request){
      $data = $request->only('table_no','floorid');
      $data['table_no']  = json_decode($data['table_no'], true);
      $data['floorid']   = json_decode($data['floorid'], true);
      $table = new \App\Slipt_table;
      $table->table_no = $data['table_no'];
      $table->floorid  = $data['floorid'];
      $table->save();
      $slipt_count=\App\Slipt_table::where('table_no',$data['table_no'])->where('floorid',$data['floorid'])->count();
      $data['sliptid']=$table->id;
      $data['slipt_count']=$slipt_count;
      $response['status']['code']=0;
      $response['status']['message']="Listing  item details";
      $response['data']=$data;
      return response()->json($response);

   }
    
    
    
    
    
}
