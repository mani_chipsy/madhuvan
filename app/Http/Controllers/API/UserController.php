<?php
namespace App\Http\Controllers\API;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\MasterUser;
use App\Models\usersProfile;
use App\Models\UserProfileImg;
use App\Country;
use App\Image_master;
use App\Fav_team;
use Validator;
use JWTAuth;
use Auth;
use Hash;
use Image;
use DB;
class UserController extends Controller {

public function terms_condition(){
  return view('Backend.terms');
}
public function privacy_policy(){
  return view('Backend.privacy_policy');
}

public function version_func($version){
  $current_version =1;
  $force_update    =1;
  $skip_update     =1;
  if($version < $force_update ){
    $response['status']["code"] = 1;
    $response['status']["message"] = "Congrats! You got a new version";
  }else if($version >= $current_version){
            $response['status']["code"] = 0;
                $response['status']["message"] = "Thank you, You are in current version";
   }else {
            $response['status']["code"] = 2;
                $response['status']["message"] = "Congrats! you got a new version";
   }
         // echo json response
         
      return response()->json($response);


}


public function refresh_func(Request $request) {
     try {
        $headers = apache_request_headers();
        $token   = $headers['Authorizations'];
        $token   = JWTAuth::refresh($token); 
      $user_id=JWTAuth::authenticate($token)->id;

         $userdata =User::where('id',$user_id)->first();

       $response['status']['code']=0;
     $response['status']['message']="Session updated successfully.";
    $response['session']['expiry_time']=1800000;
                      $response['session']['Authorizations']=$token;
                      $response['session']['email']=$userdata->email;
                      $response['session']['username']=$userdata->username;
                   
                      $response['session']['userType'] =$userdata->roleId;
     }
     catch(JWTException $e) {
       $response['status']["code"]    = 5;
           $response['status']["message"] = 'Invalid Token.';
          
     }

                      return response()->json($response);
   }
   

         public function register_func(Request $request) {
             $rules = array('email'=>'required','password' => 'required','userType' => 'required');
             $validator = Validator::make($request->all(), $rules); 
             if ($validator->fails()) {
             $response['status']['code'] = 1;
             $response['status']['message'] = $validator->messages();
             return response()->json($response);
             }       
             $data = $request->all();
             $response['status']['code'] = 0;
             $response['status']['message'] = "User Login successfully.";
           
             
             $credentials = array(
             'email' => $data['email'],
             'password' => $data['password'],
             'roleId' => $data['userType']
              ); // select required fields  
                    
               try {
                    // attempt to verify the credentials and create a token for the user
                    if (!$token = JWTAuth::attempt($credentials)) {
                        $response['status']["code"]    = 5;
                        $response['status']["message"] = 'User Name/Password wrong.';
                        return response()->json($response);
                    }else{ 
                      $response['status']['message']   = "Login successfully";
                    }
                }
                catch (JWTException $e) {
                    // something went wrong whilst attempting to encode the token
                    $response['status']["code"]    = 10;
                    $response['status']["message"] = 'Login fails ,Try again.';
                    return response()->json($response);
                }
                      $response['session']['expiry_time']=1800000;
                      $response['session']['Authorizations']=$token;
                        $response['session']['email']=$data['email'];
                      $response['session']['username']=Auth::user()->name;

                      return response()->json($response);
         }
    
        
      

}