<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use App\BillInfo;
use App\User;
use DateTime;

class DashboardControl extends Controller
{
    public function index(Request $request)
    {
        return view('Admin.pages.home');
    }

    public function settings()
    {
        return view('Admin.pages.settings');
    }
    
    public function change_settings(Request $request)
    {
        $rules     = array(
            'username' => 'required',
            'current_password' => 'required'
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }


        if (Hash::check($data['current_password'], Auth::user()->password)) {
            $user = User::find(Auth::id());

            if ($change_pass) {
                $n_password = Hash::make($data['new_password']);
                $user->password = $n_password;
            }

            $user->name = $data['username'];
            $user->save();
            // Auth::logout();
            Auth::loginUsingId(Auth::id());

            return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Current password entered is incorrect',
            ));
        }
    }
}
