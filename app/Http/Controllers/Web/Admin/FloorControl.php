<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use App\Floor_details;
use App\User;
use DateTime;

class FloorControl extends Controller
{

public function index(Request $request)
    {

     $data = Floor_details::orderBy('created_at', 'DESC')->get();
     return view('Admin.pages.floordetailes', ['items' => $data]);
       
    }
     public function add(Request $request) {
       $inputs    = $request->all();
        $rules     = array(
            'name' => 'required',
            'noOfSeats' => 'required'

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

          $data = new Floor_details;
          $data->name=$request->name;
          $data->noOfSeats=$request->noOfSeats;
          $data->save();
          if($data){

         return response()->json(array(
                    'success' => true,
                    'message' => "Category  Added successfully.",
                    'id'=>$data->id,
                    'name'=>$data->name
                    ));
     }



    }

    public function delete($id){
    	$data =Floor_details::find($id);
    	$data->delete();

    	if($data){
    		return response()->json(array(
                    'success' => true,
                    'message' => "Floor  Delete successfully."));
    	}

    }
  public function update(Request $request,$id) {
       $inputs    = $request->all();
        $rules     = array(
            'name' => 'required',
            'noOfSeats' => 'required'

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

          $data =Floor_details::find($id);
          // die(json_encode($data));
         
          $data->name=$request->name;
          $data->noOfSeats=$request->noOfSeats;
          $data->save();
          if($data){

         return response()->json(array(
                    'success' => true,
                    'message' => "Floor  Added successfully.",
                    'id'=>$data->id,
                    'name'=>$data->name
                    ));
     }



    }
}