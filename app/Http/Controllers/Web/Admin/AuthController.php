<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('Admin.login');
    }
    
    public function authlogin_func(Request $request)
    {
        $inputs    = $request->all();
        $rules     = array(
            'username' => 'required|max:255',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $creds = [
            'name'          => $data['username'],
            'password'       => $data['password'],
            // 'power'        => 1
        ];

        if (Auth::attempt($creds, false)) {
            return response()->json(array(
                'success' => true,
                'message' => "Login successfully completed"
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => "Invalid user name or password"
            ));
        }

        return response()->json(array(
            'success' => false,
            'message' => "Invalid user name or password"
        ));
    }
    
    public function signout_func()
    {
        Auth::logout();
        return redirect('/');
    }
}
