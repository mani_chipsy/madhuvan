<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use App\Category_master;
use App\User;
use DateTime;

class CategoryControl extends Controller
{

public function index(Request $request)
    {

     $data = Category_master::orderBy('created_at', 'DESC')->get();
     return view('Admin.pages.addcategory', ['items' => $data]);
       
    }
     public function add(Request $request) {
       $inputs    = $request->all();
        $rules     = array(
            'category' => 'required'

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

          $data = new Category_master;
          $data->name=$request->category;
          $data->save();
          if($data){

         return response()->json(array(
                    'success' => true,
                    'message' => "Category  Added successfully.",
                    'id'=>$data->id,
                    'name'=>$data->name
                    ));
     }



    }

    public function delete($id){
    	$data =Category_master::find($id);
    	$data->delete();

    	if($data){
    		return response()->json(array(
                    'success' => true,
                    'message' => "Category Delete successfully."));
    	}

    }
  public function update(Request $request,$id) {
       $inputs    = $request->all();
        $rules     = array(
            'category' => 'required'

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

          $data =Category_master::find($id);
          // die(json_encode($data));
          $data->name=$request->category;
          $data->save();
          if($data){

         return response()->json(array(
                    'success' => true,
                    'message' => "Category  Added successfully.",
                    'id'=>$data->id,
                    'name'=>$data->name
                    ));
     }



    }
}