<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use App\Item_master;
use App\User;
use DateTime;

class ItemMasterControl extends Controller
{
    public function item_master(Request $request)
    {

     $data = Item_master::with('cat')->orderBy('created_at', 'DESC')->paginate(10);
     
        foreach ($data as $key) {
           $key->create=date("d-m-Y",strtotime($key->created_at));
        }
        // $data = $data->paginate(10);
        // die(json_encode($data));
        return view('Admin.pages.additems', ['items' => $data]);
        // return view('Admin.pages.additems');
    }
public function item_delete($id){
     $data = Item_master::find($id);
     $data->delete();
     if($data){

         return response()->json(array(
                    'success' => true,
                    'message' => "Deleted successfully."
                    ));
     }
}

    public function edit_item($id){

         $data = Item_master::where('id',$id)->first();
         return $data;

    }
   public function  item_get(){

       $data = Item_master::with('cat')->orderBy('created_at', 'DESC')->paginate(10);
        foreach ($data as $key) {
           $key->create=date("d-m-Y",strtotime($key->created_at));
        }
         return $data;

    }

      public function update_item(Request $request,$id) {
       $inputs    = $request->all();
        $rules     = array(
            'itemname' => 'required|max:255',
            'price' => 'required',
            'category' => 'required',
            'main_category'=>'required'

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $data=Item_master::find($id);

        $data->name=$request->itemname;
        $data->price=$request->price;
        $data->category_id=$request->category;
         $data->main_category=$request->main_category;
      
         $res = $data->save();

        if ($res) {
           return response()->json(array(
                    'success' => true,
                    'message' => "Item Updated success."
                    ));

     }
    }

     public function post_item(Request $request) {
       $inputs    = $request->all();
        $rules     = array(
            'itemname' => 'required|max:255',
            'price' => 'required',
            'category' => 'required','main_category'=>'required'

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $data=new Item_master;
        $data->name=$request->itemname;
        $data->price=$request->price;
        $data->category_id=$request->category;
        $data->main_category=$request->main_category;
         $res = $data->save();

        if ($res) {
           return response()->json(array(
                    'success' => true,
                    'message' => "insert success."
                    ));

     }
    }
}
