<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['namespace' => 'Web\Admin'], function () {
    Route::get('/superadmin/logout', 'AuthController@signout_func');
    Route::group(['middleware' => ['guest']], function () {
        Route::get('/', [ 'as' => 'login', 'uses' => 'AuthController@index']);
        Route::get('/superadmin/', [ 'as' => 'login', 'uses' => 'AuthController@index']);
        Route::post('/superadmin/auth-login', 'AuthController@authlogin_func');
    });

    Route::group(['prefix' => 'superadmin'], function () {
        Route::group(['middleware' =>'auth'], function () {
            Route::get('/settings', 'DashboardControl@settings');
            Route::post('/sub-settings', 'DashboardControl@change_settings');


            Route::get('/item-master', 'ItemMasterControl@item_master');
            Route::post('/additem', 'ItemMasterControl@post_item');
             Route::get('/edititem/{id}', 'ItemMasterControl@edit_item');
              Route::post('/itemupdate/{id}', 'ItemMasterControl@update_item');
               Route::get('/get-data', 'ItemMasterControl@item_get');
               Route::get('/delete-item/{id}', 'ItemMasterControl@item_delete');
             
               Route::get('/dashboard', 'DashboardControl@index');
            
            Route::get('/dashboard/get-billings/{category}', 'DashboardControl@getData');


             Route::group(['prefix' => 'category'], function () {
                 Route::get('/', 'CategoryControl@index');
                 Route::post('/add', 'CategoryControl@add');
                 Route::post('/update/{id}', 'CategoryControl@update');
                 Route::delete('delete/{id}', 'CategoryControl@delete');


             });
            Route::group(['prefix' => 'floor-details'], function () {
                 Route::get('/', 'FloorControl@index');
                 Route::post('/add', 'FloorControl@add');
                 Route::post('/update/{id}', 'FloorControl@update');
                 Route::delete('delete/{id}', 'FloorControl@delete');
            });

            Route::group(['prefix' => 'boxes'], function () {
                Route::get('/', 'BoxControl@index');
                Route::get('/get-data', 'BoxControl@getData');
                Route::get('/view/{id}', 'BoxControl@get');
                Route::post('/update/{id}', 'BoxControl@update');
            });

        

    

           

            Route::group(['middleware' => ['admin']], function () {
                Route::group(['prefix' => 'billings'], function () {
                    Route::post('delete', 'BillingControl@delete');
                });
                Route::group(['prefix' => 'lcos'], function () {
                    Route::get('/', 'OperatorControl@index');
                    Route::get('/get-data', 'OperatorControl@getData');
                    Route::get('/export-data', 'OperatorControl@exportData');
                    // Route::get('/view/{id}', 'OperatorControl@get');
                    // Route::get('/search/{str}', 'OperatorControl@search');
                    Route::post('/update/{id}', 'OperatorControl@update');
                    Route::post('/delete', 'OperatorControl@delete');
                    Route::post('/add', 'OperatorControl@add');
                    Route::post('/add', 'OperatorControl@add');
                    Route::post('/box_add', 'OperatorControl@box_add');
                    Route::get('/lco-boxes/{id}', 'OperatorControl@get_lco_boxes');
                    Route::get('/download/{filename}', 'OperatorControl@getDownload');
                    Route::post('/upload', 'OperatorControl@up_lcos');
                });
            });
        });
    });
});
