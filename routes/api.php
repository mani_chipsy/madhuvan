<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::group(['namespace' => 'API'], function () {
   
 	Route::group(['prefix' => '{version?}'], function () {
    Route::post('/login', 'UserController@register_func');
     Route::get('/refresh', 'UserController@refresh_func');

 

      Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/get-slipts', 'OrderController@sliptget_controller');
      	Route::post('/orders', 'OrderController@order_controller');
      		Route::get('/confirm-orders', 'OrderController@confirm_order_controller');
       Route::get('/get-floor', 'OrderController@floor_controller');
       Route::get('/get-category', 'OrderController@category_master_controller');
       Route::get('/get-item', 'OrderController@item_master_controller');
       Route::post('/request-slipt', 'OrderController@sliptrequest_controller');
    });
    
    });

 });
